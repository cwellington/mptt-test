from tree.models import Node

NUM_ROOT_NODES = 200
NUM_CHILDREN_PER_ROOT_NODE = 2

def run():
    print "setting up tree"
    root_nodes = []
    for node_num in range(NUM_ROOT_NODES):
        node = Node(name="root node %d"%node_num)
        node.save()
        root_nodes.append(node)

    all_children = []
    for node in root_nodes:
        for num in range(NUM_CHILDREN_PER_ROOT_NODE):
            subnode = Node(name="child #%d of %s"%(num, node), parent=node)
            subnode.save()
            all_children.append(subnode)

    print "testing tree"
    for node in Node.objects.filter(level=0):
        assert len(node.get_children())==NUM_CHILDREN_PER_ROOT_NODE
        assert len(node.get_descendants())==NUM_CHILDREN_PER_ROOT_NODE
