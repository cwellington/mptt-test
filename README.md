# Example showing django-mptt bug #

Django-mptt's order_insertion_by breaks the tree insertion.

### What is this repository for? ###

* An example showing the broken behaviour

### How do I break it? ###

* checkout the repo
* run `make`
* test script fails

### How do I make it work? ###
* go into mptttest/tree/models.py and comment out the MPTTMeta block
* run `make clean`
* run `make`
* test script passes