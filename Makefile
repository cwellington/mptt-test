test: vp
	echo "running mptt test"
	cd mptttest && ../vp/bin/python manage.py syncdb --noinput
	cd mptttest && ../vp/bin/python manage.py runscript test

vp: requirements.txt
	virtualenv vp
	vp/bin/pip install -r requirements.txt

clean:
	rm mptttest/db.sqlite3
